package name.alp.shopping.productsservice;

import name.alp.shopping.productsservice.client.category.Category;
import name.alp.shopping.productsservice.client.category.CategoryClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ProductRestController.class)
@Import(ProductService.class)
public class ProductRestControllerTest {

    @MockBean
    ProductRepository repository;
    @MockBean
    CategoryClient categoryClient;
    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        Category category = new Category("test_category_id1", "test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Product product = new Product();
        product.setId("test_id1");
        product.setTitle("Test title");
        product.setCategory(category);

        Mockito.when(repository.save(product)).thenReturn(Mono.just(product));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(product))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById() {
        Product product = new Product();
        product.setId("test_id1");
        product.setTitle("Test title");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(product));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.title").isNotEmpty()
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }

    @Test
    void testFindAll() {
        Category category = new Category("test_category_id1", "test category");
        Product product1 = new Product();
        product1.setId("test_id1");
        product1.setTitle("Test title");
        product1.setCategory(category);
        Product product2 = new Product();
        product2.setId("test_id2");
        product2.setTitle("Test title");
        product2.setCategory(category);
        Product product3 = new Product();
        product3.setId("test_id3");
        product3.setTitle("Test title");
        product3.setCategory(category);

        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Product[]{product1, product2, product3}));

        webClient.get().uri("/")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Product.class);


    }

    @Test
    public void findByCategoryId() {
        Category category = new Category("test_category_id1", "test category");
        Product product1 = new Product();
        product1.setId("test_id2");
        product1.setTitle("Test title");
        product1.setCategory(category);
        Product product2 = new Product();
        product2.setId("test_id3");
        product2.setTitle("Test title");
        product2.setCategory(category);

        Mockito
                .when(repository.findByCategoryId(category.getId()))
                .thenReturn(Flux.fromArray(new Product[]{product1, product2}));

        webClient.get().uri("/category/", category.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Product.class);
    }
}
