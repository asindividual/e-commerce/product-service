package name.alp.shopping.productsservice;


import name.alp.shopping.productsservice.client.category.Category;
import name.alp.shopping.productsservice.client.category.CategoryClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = ProductRestController.class)
@Import(ProductService.class)
public class ProductServiceTest {

    @MockBean
    ProductRepository repository;
    @MockBean
    CategoryClient categoryClient;
    @Autowired
    private ProductService productService;

    @Test
    public void saveTest() {
        Category category = new Category("test_category_id1", "test category");
        Mockito.when(categoryClient.findById(category.getId())).thenReturn(category);

        Product product = new Product();
        product.setId("test_id1");
        product.setTitle("Test title");
        product.setCategory(category);
        Mono<Product> expected = Mono.just(product);
        Mockito
                .when(repository.save(product))
                .thenReturn(expected);


        Mono<Product> actual = productService.save(product);
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void findById() {
        Product product = new Product();
        product.setId("test_id1");
        product.setTitle("Test title");
        Mono<Product> expected = Mono.just(product);

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(expected);

        Mono<Product> actual = productService.findById("test_id1");
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    public void findAllTest() {
        Product product1 = new Product();
        product1.setId("test_id1");
        product1.setTitle("Test title");
        Product product2 = new Product();
        product2.setId("test_id2");
        product2.setTitle("Test title");
        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Product[]{product1, product2}));

        Flux<Product> result = productService.findAll();
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }

    @Test
    public void findByCategoryId() {
        Category category = new Category("test_category_id1", "test category");
        Product product1 = new Product();
        product1.setId("test_id2");
        product1.setTitle("Test title");
        product1.setCategory(category);
        Product product2 = new Product();
        product2.setId("test_id3");
        product2.setTitle("Test title");
        product2.setCategory(category);

        Mockito
                .when(repository.findByCategoryId(category.getId()))
                .thenReturn(Flux.fromArray(new Product[]{product1, product2}));

        Flux<Product> result = productService.findByCategoryId(category.getId());
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }
}
