package name.alp.shopping.productsservice.exceptions;

public class CategoryNotFoundException extends RuntimeException {
    public CategoryNotFoundException(String categoryId) {
        super("Category Not Found path:" + categoryId);
    }
}
