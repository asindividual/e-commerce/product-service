package name.alp.shopping.productsservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ProductRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductRestController.class);
    private final ProductService productService;

    public ProductRestController(ProductService productService) {
        this.productService = productService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates")
    @PostMapping
    public Mono<Product> save(@RequestBody Product product) {
        LOGGER.info("create: {}", product);
        return productService.save(product);
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates")
    @GetMapping("/{id}")
    public Mono<Product> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return productService.findById(id);
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates")
    @GetMapping
    public Flux<Product> findAll() {
        LOGGER.info("findAll");
        return productService.findAll();
    }


    @GetMapping("/category/{category}")
    public Flux<Product> findByCategoryId(@PathVariable("category") String categoryId) {
        LOGGER.info("findByCategoryId: category={}", categoryId);
        return productService.findByCategoryId(categoryId);
    }


}
