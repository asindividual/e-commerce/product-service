package name.alp.shopping.productsservice;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface ProductRepository extends ReactiveCrudRepository<Product, String> {
    @Query("{'category.id': ?0 ")
    Flux<Product> findByCategoryId(String categoryId);
}
