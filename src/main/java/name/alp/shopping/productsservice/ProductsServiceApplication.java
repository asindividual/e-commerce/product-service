package name.alp.shopping.productsservice;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
@EnableDiscoveryClient
public class ProductsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductsServiceApplication.class, args);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${info.app.maven.description}") String appDesciption, @Value("${info.app.maven.version}") String appVersion) {
        return new OpenAPI()
                .info(new Info()
                        .title("Products API for Shopping application")
                        .version(appVersion)
                        .description(appDesciption)
                        .termsOfService("")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }
}
