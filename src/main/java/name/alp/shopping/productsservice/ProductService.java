package name.alp.shopping.productsservice;

import name.alp.shopping.productsservice.client.category.Category;
import name.alp.shopping.productsservice.client.category.CategoryClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductService {

    private final ProductRepository productRepository;
    private final CategoryClient categoryClient;

    public ProductService(ProductRepository productRepository, @Lazy CategoryClient categoryClient) {
        this.productRepository = productRepository;
        this.categoryClient = categoryClient;
    }

    public Mono<Product> save(Product product) {
        Category category = categoryClient.findById(product.getCategory().getId());
        product.setCategory(category);
        Mono<Product> result = productRepository.save(product);

        return result;
    }

    public Mono<Product> findById(String id) {
        return productRepository.findById(id);
    }


    public Flux<Product> findAll() {
        return productRepository.findAll();
    }

    public Flux<Product> findByCategoryId(String categoryId) {
        return productRepository.findByCategoryId(categoryId);
    }
}
